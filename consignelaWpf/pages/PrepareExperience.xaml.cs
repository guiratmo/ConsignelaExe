﻿using consignelaWpf.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using static System.Environment;

namespace consignelaWpf.pages
{
    /// <summary>
    /// Interaction logic for PrepareExperience.xaml
    /// </summary>
    public partial class PrepareExperience : Page
    {
        public String btnLocalServeurChoisi = "";
        public String bouttonChoisi = "";
        internal String str_Consigne_json;
        internal String str_Consigne_jsonServeur;

        internal List<ListConsignes> listListConsignes;
        internal List<ListConsignes> listListConsignesServeur;
        internal List<Consigne> listConsigneTmp = new List<Consigne>();

        internal String nomConsigne;
        internal bool consChoisie = false;
        internal String msgFeedback;
        internal ListConsignes listConstmp;
        private ListConsignes listConstmp2 = new ListConsignes();
        int indexConstmp;

        private ObservableCollection<Consigne> ListLignesConsignes = new ObservableCollection<Consigne>();
        private Collection<Consigne> listConsignetmpCollection = new Collection<Consigne>();

        private ObservableCollection<Consigne> ListLignesConsignesModif = new ObservableCollection<Consigne>();
        private Collection<Consigne> ListLignesConsignesModifCollec = new Collection<Consigne>();

     
        private int index, i, indexDebut, indexFin;
        internal Consigne consigneTmp = new Consigne();
        internal static String imgPath = null;
        internal static String imgNom = null;
        internal String TxtResponse;
        //public static StorageFile sampleFile;

        List<string> nameList = new List<string>();
        List<string> elementList = new List<string>();
        List<CheckBox> checkBoxListSharingSpace = new List<CheckBox>();
        public static List<CheckBox> checkBoxListIndex = new List<CheckBox>();

        static List<string> idSharingSpace = new List<string>();
        byte[] bytes;
        //public StorageFile downloadedFile;
        public int indeximages = 0;
        public bool searchName = false, searchDate = false;

        //*****************Prescription

        List<Prescription> newListPrescriptions;
        List<Medicament> newMedicament;
        internal String strJsonPresc;
        internal bool prescChoisie = false;
        internal String nomPrescription;

        internal List<Medicament> tabListMedicaments;
        internal List<PrescriptionTab> prescriptionTabList;
        internal String strJsonTabPresc;
        //internal String nomMomentJour;
        internal List<ListView> gridViewTabMedicament = new List<ListView>();
        internal Prescription prescriptionTmp;
        internal String sampleFilePrescription;
        public static String sampleFilePrescriptionTab;


        internal List<ExperienceConf> ListExperiencesConf;
        internal List<ExperienceConf> ListExperiencesConfTmp;
        internal ExperienceConf experienceConfTmp;
        //public static StorageFile sampleFileExperienceConf;
        internal String strJsonExpConf;
        internal int indexExp;

        //**************
        String sampleFile;
        String sampleFileServeur;
        string pathFolder = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
        

        public PrepareExperience()
        {
            InitializeComponent();
            initialiserAffichage();
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {           
            //Process.Start(sampleFile);           
            var storageDirectory = System.IO.Path.Combine(pathFolder, "Consignela");
            Directory.CreateDirectory(storageDirectory);
             sampleFile = System.IO.Path.Combine(storageDirectory, "ConsignesData.json");


            //File.Create(sampleFile).Close();
            //File.Open(sampleFile, FileMode.Open, FileAccess.ReadWrite);
            System.Diagnostics.Debug.WriteLine(sampleFile);
            chargerDonnees();
            buttonLocalCliked();
            loadPrescription();
            loadPrescriptionTabList();
            verifConneted();
            loadGridView();
        }
        public string getJsonFromUrl(string url)
        {
            using (WebClient wc = new WebClient())
            {
                var json = wc.DownloadString(url);
                return json;
            }
        }
        public void chargerDonneesServeur()
        {
            sampleFileServeur = getJsonFromUrl("http://memorae.hds.utc.fr/api/documents/brahim-consignes.json");

            if (!String.IsNullOrEmpty(sampleFileServeur))
            {
                str_Consigne_jsonServeur = sampleFileServeur;
                listListConsignesServeur = JsonConvert.DeserializeObject<List<ListConsignes>>(str_Consigne_jsonServeur);
  
                if (listListConsignesServeur.Count != 0)
                {
                    i = listListConsignesServeur[listListConsignesServeur.Count - 1].id + 1;
                }
                else
                {
                    i = 0;
                }
                rechargerListViewConsigneServeur();
            }


        }
        public void verifConneted()
        {
            chargerDonneesServeur();
            if (App.authentification)
            {
                btnAfficheServeur.Visibility = Visibility.Visible;
                chargerDonneesServeur();
            }
           
        }
        public void chargerDonnees()
        {
            if (File.Exists(sampleFile))
            {
                str_Consigne_json = File.ReadAllText(sampleFile);
                listListConsignes = JsonConvert.DeserializeObject<List<ListConsignes>>(str_Consigne_json);
                System.Diagnostics.Debug.WriteLine(listListConsignes);              
                //using (StreamWriter sw = new StreamWriter(sampleFile))
                //{
                //    sw.Write("data");

                //}
                if (listListConsignes.Count != 0)
                {
                    i = listListConsignes[listListConsignes.Count - 1].id + 1;
                }
                else
                {
                    i = 0;
                }
                rechargerListViewConsigne();
            }
        }
        public void rechargerListViewConsigneServeur()
        {
            ListViewConsiServeur.ItemsSource = null;
            ListViewConsiServeur.ItemsSource = listListConsignesServeur;
        }
        public void rechargerListViewConsigne()
        {
            gridViewConsi.ItemsSource = null;
            gridViewConsi.ItemsSource = listListConsignes;
        }
        public void rechargerListViewLignesConsigne()
        {
            gridViewConsignes.ItemsSource = null;
            gridViewConsignes.ItemsSource = listConsigneTmp;
        }

        private void initialiserAffichage()
        {
            modify.IsEnabled = false;
            //this.delete.IsEnabled = false;
            tb_nomConsigne.Text = "Nom de la liste de Consigne";
            gridViewConsignes.ItemsSource = null;
            afficheListeConsigne.Visibility = Visibility.Collapsed;

        }

        private void preparerExperience_Click(object sender, RoutedEventArgs e)
        {
            panelPreparer.Visibility = Visibility.Visible;
            panelChoixOuPrep.Visibility = Visibility.Collapsed;
        }

        private void retour_Click(object sender, RoutedEventArgs e)
        {
            panelPreparer.Visibility = Visibility.Collapsed;
            panelChoixOuPrep.Visibility = Visibility.Visible;
            selectionConsignePanel.Visibility = Visibility.Collapsed;
            SelectionPrescription.Visibility = Visibility.Collapsed;
        }
        public void consigneChoisie()
        {
            textSelectConsiBtn.Text = "Liste des consignes choisie : " + nomConsigne;
            selectionConsigne.Background = new SolidColorBrush(Colors.White);
            selectionConsigne.BorderBrush = new SolidColorBrush(Color.FromArgb(255, 253, 153, 8));
            selectionConsigne.BorderThickness = new Thickness(2.0);
            textSelectConsiBtn.Foreground = new SolidColorBrush(Color.FromArgb(255, 253, 153, 8));
        }
        private void selection_Click(object sender, RoutedEventArgs e)
        {
            if (consChoisie)
            {             
                App.consignesSelect = listListConsignes[listConstmp.id];
                msgFeedback = "la consigne " + nomConsigne + " a bien été  choisie";
                consigneChoisie();
                selectionPrescription.IsEnabled = true;
            }
            else
            {
                msgFeedback = "La Consigne est non choisise";
            }
        }

        private void btn_Save_Click(object sender, RoutedEventArgs e)
        {

        }

        private void modify_Click(object sender, RoutedEventArgs e)
        {
            popupConsigneNom.Text = listListConsignes[listConstmp.id].nom;
            if (!ModifierPopup.IsOpen) { ModifierPopup.IsOpen = true; }
            //ModifierPopup.Height = Window.Current.Bounds.Height;
            selectionConsignePanel.Opacity = 0.5;
            //barSelection.IsEnabled = false;
            gridViewConsi.IsEnabled = false;
            searchWithName.IsEnabled = false;
        }

     

        private void delete_Click(object sender, RoutedEventArgs e)
        {
            listListConsignes.Remove(listConstmp);
            int index = 0;
            foreach (ListConsignes listConsigneD in listListConsignes)
            {
                listConsigneD.id = index;
                index++;
            }
            gridViewConsi.ItemsSource = null;
            gridViewConsi.ItemsSource = listListConsignes;

            MessageBoxResult result = MessageBox.Show("Attention voulez-vous vraiment supprimer cette consigne ?", "Suppression de la liste consigne", MessageBoxButton.YesNo);
            switch (result)
            {
                case MessageBoxResult.Yes:
                    supprimerConsigne();
                    break;
                case MessageBoxResult.No:
                    //MessageBox.Show("Oh well, too bad!", "My App");
                    break;               
            }
            //MessageDialog deleteConsigne = new MessageDialog("Attention voulez-vous vraiment supprimer cette consigne ?", "Suppression de la consigne");
            //deleteConsigne.Commands.Add(new UICommand("Supprimer", supprimerConsigne, 1));
            //deleteConsigne.Commands.Add(new UICommand("Annuler", CancelCommand, 2));
            //await deleteConsigne.ShowAsync();
        }

        public void supprimerConsigne()
        {
            listListConsignes.Remove(listConstmp);
            int index = 0;
            foreach (ListConsignes listConsigneD in listListConsignes)
            {
                listConsigneD.id = index;
                index++;
            }
            gridViewConsi.ItemsSource = null;
            gridViewConsi.ItemsSource = listListConsignes;
            initialiserAffichage();

            File.WriteAllText(sampleFile, JsonConvert.SerializeObject(listListConsignes));
        }

        private void recherche_click(object sender, RoutedEventArgs e)
        {
            if (bouttonChoisi == "ListeConsignes")
            {
                if (searchName && !String.IsNullOrEmpty(searchWithName.Text))
                {
                    if (!String.IsNullOrEmpty(searchWithName.Text))
                    {
                        gridViewConsi.ItemsSource = null;

                        gridViewConsi.ItemsSource = listListConsignes.FindAll(listConsigne => listConsigne.nom.Contains(searchWithName.Text));
                        // System.Diagnostics.Debug.Write(gridViewConsi);
                    }
                    else
                    {
                        gridViewConsi.ItemsSource = null;
                        gridViewConsi.ItemsSource = listListConsignes;
                    }

                }
                if (searchDate)
                {
                    bool vd = true, vf = true;

                    foreach (ListConsignes listConsi in listListConsignes)
                    {
                        if ((DateTime.Compare(listConsi.dateTime.Date, dateDebut.SelectedDate.Value) > 0 || DateTime.Compare(listConsi.dateTime.Date, dateDebut.SelectedDate.Value) == 0) && vd)
                        {
                            indexDebut = listConsi.id;
                            vd = false;
                        }

                        if ((DateTime.Compare(listConsi.dateTime.Date, dateFin.SelectedDate.Value) < 0 || DateTime.Compare(listConsi.dateTime.Date, dateFin.SelectedDate.Value) == 0))
                        {
                            indexFin = listConsi.id + 1;
                            //vf = false;
                            //break;
                        }
                    }
                    List<ListConsignes> listListConsiTmp = listListConsignes.Skip(indexDebut).Take(indexFin - indexDebut).Cast<ListConsignes>().ToList();

                    System.Diagnostics.Debug.WriteLine(dateDebut.SelectedDate.Value);

                    gridViewConsi.ItemsSource = null;
                    gridViewConsi.ItemsSource = listListConsiTmp;

                }
            }
            if (bouttonChoisi == "Prescription")
            {
                if (searchName && !String.IsNullOrEmpty(searchWithName.Text))
                {
                    if (!String.IsNullOrEmpty(searchWithName.Text))
                    {
                        gridViewPresc.ItemsSource = null;

                        gridViewPresc.ItemsSource = newListPrescriptions.FindAll(prescriptionSearch => prescriptionSearch.nom.Contains(searchWithName.Text));
                        // System.Diagnostics.Debug.Write(gridViewConsi);
                    }
                    else
                    {
                        gridViewPresc.ItemsSource = null;
                        gridViewPresc.ItemsSource = listListConsignes;
                    }

                }
                if (searchDate)
                {
                    bool vd = true, vf = true;

                    foreach (Prescription prescriptionSearch in newListPrescriptions)
                    {
                        if ((DateTime.Compare(prescriptionSearch.dateTime.Date, dateDebut.SelectedDate.Value) > 0 || DateTime.Compare(prescriptionSearch.dateTime.Date, dateDebut.SelectedDate.Value) == 0) && vd)
                        {
                            indexDebut = prescriptionSearch.id;
                            vd = false;
                        }

                        if ((DateTime.Compare(prescriptionSearch.dateTime.Date, dateFin.SelectedDate.Value) < 0 || DateTime.Compare(prescriptionSearch.dateTime.Date, dateFin.SelectedDate.Value) == 0))
                        {
                            indexFin = prescriptionSearch.id + 1;
                            //vf = false;
                            //break;
                        }
                    }
                    List<Prescription> listPrescriptionTmp = newListPrescriptions.Skip(indexDebut).Take(indexFin - indexDebut).Cast<Prescription>().ToList();

                    System.Diagnostics.Debug.WriteLine(dateDebut.SelectedDate.Value);

                    gridViewPresc.ItemsSource = null;
                    gridViewPresc.ItemsSource = listPrescriptionTmp;

                }
            }



            if (shearchPopup.IsOpen)
            {
                shearchPopup.IsOpen = false;
            }

        }

        private void RechercheListe_Click(object sender, RoutedEventArgs e)
        {
            if (!shearchPopup.IsOpen) { shearchPopup.IsOpen = true; }
        }

        private void dateDebut_Tapped(object sender, RoutedEventArgs e)
        {
            searchName = false;
            searchDate = true;
            searchWithName.Text = "";
            stackPanelSearch.Background = new SolidColorBrush(Colors.DarkGray);
            relativePanelDate.Background = new SolidColorBrush(Colors.WhiteSmoke);
        }

        private void dateFin_Tapped(object sender, RoutedEventArgs e)
        {
            searchName = false;
            searchDate = true;
            searchWithName.Text = "";
            stackPanelSearch.Background = new SolidColorBrush(Colors.DarkGray);
            relativePanelDate.Background = new SolidColorBrush(Colors.WhiteSmoke);
        }

        private void searchConsigne_GotFocus(object sender, RoutedEventArgs e)
        {           
           searchWithName.Text = "";

            searchName = true;
            searchDate = false;
            stackPanelSearch.Background = new SolidColorBrush(Color.FromArgb(255, 230, 230, 230));
            relativePanelDate.Background = new SolidColorBrush(Colors.DarkGray);
        }

        private void searchWithName_LostFocus(object sender, RoutedEventArgs e)
        {
            searchWithName.Text = "Recherche par nom";
        }

        private void quitSearchPopup_Click(object sender, RoutedEventArgs e)
        {
            if (shearchPopup.IsOpen)
            {
                shearchPopup.IsOpen = false;
            }
        }

        private void ajoutListeConsignebtn_Click(object sender, RoutedEventArgs e)
        {
            if (!ajoutPopup.IsOpen) { ajoutPopup.IsOpen = true; }
            selectionConsignePanel.Opacity = 0.1;
            //barSelection.IsEnabled = false;
            gridViewConsi.IsEnabled = false;

            searchWithName.IsEnabled = false;
        }

        private void popupAJoutConsigneNom_GotFocus(object sender, RoutedEventArgs e)
        {
            popupAJoutConsigneNom.Text = "";
        }

        private void chargerListConsignetmp()
        {

            if (ListLignesConsignes != null)
            {
                listConsignetmpCollection = ListLignesConsignes;
            }
            listViewListConsigne.ItemsSource = null;
            listViewListConsigne.ItemsSource = listConsignetmpCollection;

        }
        private void btn_ajoutLigneConsigne_Click(object sender, RoutedEventArgs e)
        {
            chargerListConsignetmp();
            System.Diagnostics.Debug.Write(listConsignetmpCollection);
            if (listConsignetmpCollection.Count() != 0)
            {
                index = listConsignetmpCollection[listConsignetmpCollection.Count - 1].id + 1;
            }
            else
            {
                index = 0;
            }

            Consigne ligneConsigne = new Consigne { id = index, description = "", img_path = "Assets/icons/addimg.png" };
            ListLignesConsignes.Add(ligneConsigne);
            index++;
        }

        private void ajoutConsSave_Click(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrEmpty(popupAJoutConsigneNom.Text))
            {
                Date date = new Date();
                date.day = DateTime.Today.Day.ToString();
                date.month = DateTime.Today.Month.ToString();
                date.month = date.monthToStr();
                date.year = DateTime.Today.Year.ToString();

                ListConsignes newListConsignes = new ListConsignes { id = i, nom = popupAJoutConsigneNom.Text, date = DateTime.Today.Date.ToString("dd-MM-yyyy"), dateTime = DateTime.Today.Date, dateLettre = date };

                newListConsignes.consignes = listConsignetmpCollection.Cast<Consigne>().ToList();
                listListConsignes.Add(newListConsignes);
                i++;
                gridViewConsi.ItemsSource = null;
                gridViewConsi.ItemsSource = listListConsignes;

                File.WriteAllText(sampleFile, JsonConvert.SerializeObject(listListConsignes));
                //File.WriteAllText(@"ConsignesData.json", JsonConvert.SerializeObject(listListConsignes));
            }
            if (ajoutPopup.IsOpen) { ajoutPopup.IsOpen = false; }
            selectionConsignePanel.Opacity = 1;
            //barSelection.IsEnabled = true;
            gridViewConsi.IsEnabled = true;

            searchWithName.IsEnabled = true;
        }

        private void CloseAjoutPopupClicked(object sender, RoutedEventArgs e)
        {
            if (ajoutPopup.IsOpen) { ajoutPopup.IsOpen = false; }
            selectionConsignePanel.Opacity = 1;
            //barSelection.IsEnabled = true;
            gridViewConsi.IsEnabled = true;

            searchWithName.IsEnabled = true;
        }

        private void modifConsSave_Click(object sender, RoutedEventArgs e)
        {
            listConstmp = listConstmp2;
            listConstmp.nom = popupConsigneNom.Text;
            tb_nomConsigne.Text = popupConsigneNom.Text;
            //listConstmp.consignes[0].description = ListViewConsignes.SelectedItem.ToString();
            listConstmp.consignes = ListViewConsignesModif.Items.OfType<Consigne>().ToList();

            listListConsignes[listConstmp.id] = listConstmp;

            rechargerListViewConsigne();
            rechargerListViewLignesConsigne();

            ClosePopupClicked(sender, e);
            File.WriteAllText(sampleFile, JsonConvert.SerializeObject(listListConsignes));
        }

        private void enregistrerSous_Click(object sender, RoutedEventArgs e)
        {
            listConstmp.nom = popupConsigneNom.Text;
            tb_nomConsigne.Text = popupConsigneNom.Text;
            //listConstmp.consignes[0].description = ListViewConsignes.SelectedItem.ToString();
            listConstmp.consignes = ListViewConsignesModif.Items.OfType<Consigne>().ToList();

            listListConsignes.Add(listConstmp);
            listListConsignes[listListConsignes.Count - 1].id = listListConsignes.Count - 1;

            listListConsignes[indexConstmp] = listConstmp2;
            rechargerListViewConsigne();
            rechargerListViewLignesConsigne();

            ClosePopupClicked(sender, e);
            File.WriteAllText(sampleFile, JsonConvert.SerializeObject(listListConsignes));
        }

        private void ClosePopupClicked(object sender, RoutedEventArgs e)
        {
            if (ModifierPopup.IsOpen) { ModifierPopup.IsOpen = false; }
            selectionConsignePanel.Opacity = 1;
            //barSelection.IsEnabled = true;
            gridViewConsi.IsEnabled = true;

            searchWithName.IsEnabled = true;
        }

        private void up_Tapped_modif(object sender, RoutedEventArgs e)
        {
            consigneTmp = (Consigne)ListViewConsignesModif.SelectedItem;
            if (consigneTmp != null)
            {
                int indexConsigne = ListLignesConsignesModif.IndexOf(consigneTmp);
                if (indexConsigne > 0)
                {
                    ListLignesConsignesModif[indexConsigne].id--;
                    ListLignesConsignesModif[indexConsigne - 1].id++;

                    ListLignesConsignesModif[indexConsigne] = ListLignesConsignesModif[indexConsigne - 1];
                    ListLignesConsignesModif[indexConsigne - 1] = consigneTmp;



                    ListViewConsignesModif.ItemsSource = null;
                    ListViewConsignesModif.ItemsSource = ListLignesConsignesModif;
                }
            }
               
        }

        private void down_Tapped_modif(object sender, RoutedEventArgs e)
        {
            consigneTmp = (Consigne)ListViewConsignesModif.SelectedItem;
            if(consigneTmp != null)
            {
                int indexConsigne = ListLignesConsignesModif.IndexOf(consigneTmp);

                if (indexConsigne < ListLignesConsignesModif.Count() - 1)
                {
                    ListLignesConsignesModif[indexConsigne].id++;
                    ListLignesConsignesModif[indexConsigne + 1].id--;

                    ListLignesConsignesModif[indexConsigne] = ListLignesConsignesModif[indexConsigne + 1];
                    ListLignesConsignesModif[indexConsigne + 1] = consigneTmp;



                    ListViewConsignesModif.ItemsSource = null;
                    ListViewConsignesModif.ItemsSource = ListLignesConsignesModif;
                }
            }
            
        }

        private void deleteModif_Tapped(object sender, RoutedEventArgs e)
        {
            consigneTmp = (Consigne)ListViewConsignesModif.SelectedItem;
            if (consigneTmp != null)
            {
                if (listConstmp2.consignes.Count > 1)
                {

                    listConstmp2.consignes.Remove(consigneTmp);
                    int indexC = 0;
                    foreach (Consigne consigne in listConstmp2.consignes)
                    {
                        consigne.id = indexC;
                        indexC++;
                    }
                    ListViewConsignesModif.ItemsSource = null;
                    ListViewConsignesModif.ItemsSource = listConstmp2.consignes;

                }
            }
        }

        private void up_Tapped(object sender, RoutedEventArgs e)
        {
            consigneTmp = (Consigne)listViewListConsigne.SelectedItem;
            int indexConsigne = listConsignetmpCollection.IndexOf(consigneTmp);
            if (indexConsigne > 0)
            {
                listConsignetmpCollection[indexConsigne].id--;
                listConsignetmpCollection[indexConsigne - 1].id++;

                listConsignetmpCollection[indexConsigne] = listConsignetmpCollection[indexConsigne - 1];
                listConsignetmpCollection[indexConsigne - 1] = consigneTmp;

                listViewListConsigne.ItemsSource = null;
                listViewListConsigne.ItemsSource = listConsignetmpCollection;
            }
        }

        private void down_Tapped(object sender, RoutedEventArgs e)
        {
            consigneTmp = (Consigne)listViewListConsigne.SelectedItem;
            int indexConsigne = listConsignetmpCollection.IndexOf(consigneTmp);

            if (indexConsigne < listConsignetmpCollection.Count() - 1)
            {
                listConsignetmpCollection[indexConsigne].id++;
                listConsignetmpCollection[indexConsigne + 1].id--;

                listConsignetmpCollection[indexConsigne] = listConsignetmpCollection[indexConsigne + 1];
                listConsignetmpCollection[indexConsigne + 1] = consigneTmp;



                listViewListConsigne.ItemsSource = null;
                listViewListConsigne.ItemsSource = listConsignetmpCollection;
            }
        }

        private void deleteAjout_Tapped(object sender, RoutedEventArgs e)
        {
            consigneTmp = (Consigne)listViewListConsigne.SelectedItem;
            if (listConsignetmpCollection.Count > 1)
            {

                listConsignetmpCollection.Remove(consigneTmp);
                int indexC = 0;
                foreach (Consigne consigne in listConsignetmpCollection)
                {
                    consigne.id = indexC;
                    indexC++;
                }
                listViewListConsigne.ItemsSource = null;
                listViewListConsigne.ItemsSource = listConsignetmpCollection;

            }
        }

        private void selectionConsigne_Click(object sender, RoutedEventArgs e)
        {
            bouttonChoisi = "ListeConsignes";
            selectionConsignePanel.Visibility = Visibility.Visible;
            SelectionPrescription.Visibility = Visibility.Collapsed;
        }

        public void loadGridView()
        {
            gridViewTabMedicament.Add(gridViewTabMedicament11);
            gridViewTabMedicament.Add(gridViewTabMedicament12);
            gridViewTabMedicament.Add(gridViewTabMedicament13);
            gridViewTabMedicament.Add(gridViewTabMedicament14);
            gridViewTabMedicament.Add(gridViewTabMedicament15);
            gridViewTabMedicament.Add(gridViewTabMedicament16);
            gridViewTabMedicament.Add(gridViewTabMedicament17);
            gridViewTabMedicament.Add(gridViewTabMedicament21);
            gridViewTabMedicament.Add(gridViewTabMedicament22);
            gridViewTabMedicament.Add(gridViewTabMedicament23);
            gridViewTabMedicament.Add(gridViewTabMedicament24);
            gridViewTabMedicament.Add(gridViewTabMedicament25);
            gridViewTabMedicament.Add(gridViewTabMedicament26);
            gridViewTabMedicament.Add(gridViewTabMedicament27);
            gridViewTabMedicament.Add(gridViewTabMedicament31);
            gridViewTabMedicament.Add(gridViewTabMedicament32);
            gridViewTabMedicament.Add(gridViewTabMedicament33);
            gridViewTabMedicament.Add(gridViewTabMedicament34);
            gridViewTabMedicament.Add(gridViewTabMedicament35);
            gridViewTabMedicament.Add(gridViewTabMedicament36);
            gridViewTabMedicament.Add(gridViewTabMedicament37);
        }
        private void selectionPrescription_Click(object sender, RoutedEventArgs e)
        {
            bouttonChoisi = "Prescription";
            SelectionPrescription.Visibility = Visibility.Visible;
            selectionConsignePanel.Visibility = Visibility.Collapsed;
        }

        private void tgb_Verbale_Click(object sender, RoutedEventArgs e)
        {
            if (tgb_Verbale.IsChecked == true)
            {

                prescTabulaire.Opacity = 0;
                listViewSelectPresc.Opacity = 1;
                tgb_tabulaire.IsChecked = false;
            }
        }

        private void tgb_tabulaire_Click(object sender, RoutedEventArgs e)
        {
            if (tgb_tabulaire.IsChecked == true)
            {

                prescTabulaire.Opacity = 1;
                listViewSelectPresc.Opacity = 0;
                tgb_Verbale.IsChecked = false;
            }
        }
        public void prescriptionChoisie()
        {
            textSelectPrescBtn.Text = "Prescription choisie : " + nomPrescription;
            selectionPrescription.Background = new SolidColorBrush(Colors.White);
            selectionPrescription.BorderBrush = new SolidColorBrush(Color.FromArgb(255, 253, 153, 8));
            selectionPrescription.BorderThickness = new Thickness(2.0);
            textSelectPrescBtn.Foreground = new SolidColorBrush(Color.FromArgb(255, 253, 153, 8));
        }
        private void selectionPrescBtn_Click(object sender, RoutedEventArgs e)
        {
            if (prescChoisie)
            {
              
                App.prescriptionSelect = newListPrescriptions[prescriptionTmp.id];
                App.prescriptionTabSelect = prescriptionTabList[prescriptionTmp.id];
                prescriptionChoisie();
                LancerExperience.IsEnabled = true;
              
            }
            else
            {
   
            }
        }

        private void btn_SavePresc_Click(object sender, RoutedEventArgs e)
        {

        }

        private void gridViewPresc_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            info_label.Visibility = Visibility.Collapsed;
            typePresButton.Visibility = Visibility.Visible;

            prescriptionTmp = (Prescription)(sender as ListView).SelectedItem;
            if(prescriptionTmp !=null)
            {
                newMedicament = newListPrescriptions[prescriptionTmp.id].medicaments;

                listViewSelectPresc.ItemsSource = null;
                listViewSelectPresc.ItemsSource = newMedicament;

                gridViewNomsMedicaments.ItemsSource = null;
                gridViewNomsMedicaments.ItemsSource = newMedicament;

                nomPrescription = newListPrescriptions[prescriptionTmp.id].nom;
                tbNomPrescription.Text = nomPrescription;
                //tb_nomCons_bar.Text = nomPrescription;

                for (int i = 0; i < 21; i++)
                {
                    if ((prescriptionTabList[prescriptionTmp.id].momentJour[i].listMedicaments) != null)
                    {
                        tabListMedicaments = prescriptionTabList[prescriptionTmp.id].momentJour[i].listMedicaments;
                        gridViewTabMedicament[i].ItemsSource = null;
                        gridViewTabMedicament[i].ItemsSource = tabListMedicaments;
                    }
                }
                prescChoisie = true;
            }
        }
        public void buttonLocalCliked ()
        {
            btnLocalServeurChoisi = "local";
            btnAfficheServeur.Background = new SolidColorBrush(Colors.LightGray);
            btnAfficheLocal.Background = new SolidColorBrush(Color.FromArgb(255, 0, 87, 127));
            btnAfficheLocal.Foreground = new SolidColorBrush(Colors.White);
        }
        public void buttonSeveurCliked()
        {
            btnAfficheServeur.Foreground = new SolidColorBrush(Colors.White);
            btnAfficheLocal.Background = new SolidColorBrush(Colors.LightGray);
            btnAfficheServeur.Background = new SolidColorBrush(Color.FromArgb(255, 0, 87, 127));
        }
        private void btnAfficheLocal_Click(object sender, RoutedEventArgs e)
        {
            buttonLocalCliked();
            btnLocalServeurChoisi = "local";
            gridViewConsi.Visibility = Visibility.Visible;
            ListViewConsiServeur.Visibility = Visibility.Collapsed;
        }

        private void btnAfficheServeur_Click(object sender, RoutedEventArgs e)
        {
            buttonSeveurCliked();
            btnLocalServeurChoisi = "serveur";
            gridViewConsi.Visibility = Visibility.Collapsed;
            ListViewConsiServeur.Visibility = Visibility.Visible;
        }

     
        private void gridViewConsi_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {

        }

        private void LancerExperience_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new Uri("pages/PilotExpPage.xaml", UriKind.Relative));
        }

        private void gridViewConsi_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            int indexConstmp;
             listConstmp = (ListConsignes)(sender as ListView).SelectedItem;
            if (listConstmp != null)
            {
                listConstmp2 = listConstmp;
                modify.IsEnabled = true;
                if(btnLocalServeurChoisi == "local")
                {
                    indexConstmp = listListConsignes.IndexOf(listConstmp);
                    listConsigneTmp = listListConsignes[listConstmp.id].consignes;
                    nomConsigne = listListConsignes[listConstmp.id].nom;
                    tb_nomConsigne.Text = nomConsigne;
                }
                else if (btnLocalServeurChoisi == "serveur")
                {
                     indexConstmp = listListConsignesServeur.IndexOf(listConstmp);
                    listConsigneTmp = listListConsignesServeur[listConstmp.id].consignes;
                    nomConsigne = listListConsignesServeur[listConstmp.id].nom;
                    tb_nomConsigne.Text = nomConsigne;
                }
                    
                ListLignesConsignesModif = new ObservableCollection<Consigne>(listConsigneTmp);              
                gridViewConsignes.ItemsSource = null;
                gridViewConsignes.ItemsSource = listConsigneTmp;
                afficheListeConsigne.Visibility = Visibility.Visible;

                ListViewConsignesModif.ItemsSource = ListLignesConsignesModif;
                consChoisie = true;
            }
        }

        //*****************************Prescription **************************

        private void initialiserAffichagePrescription()
        {
            tgb_tabulaire.IsChecked = false;
            tgb_Verbale.IsChecked = true;

            prescTabulaire.Opacity = 0;
            typePresButton.Visibility = Visibility.Collapsed;
        }

        public void loadPrescriptionTabList()
        {
          
        
            var storageDirectory = System.IO.Path.Combine(pathFolder, "Consignela");
            Directory.CreateDirectory(storageDirectory);
          
            sampleFilePrescriptionTab = System.IO.Path.Combine(storageDirectory, "PrescriptionTabData.json");

            if (File.Exists(sampleFilePrescriptionTab))
            {
                strJsonTabPresc = File.ReadAllText(sampleFilePrescriptionTab);
                prescriptionTabList = JsonConvert.DeserializeObject<List<PrescriptionTab>>(strJsonTabPresc);
            }
              
        }

        public void loadPrescription()
        {
            initialiserAffichagePrescription();

            //if (App.authentification)
            //{

            //    sampleFilePrescription = await App.storageFolder.GetFileAsync(App.logIn + "-PrescriptionData.json");
            //    strJsonPresc = getJsonFromUrl("http://memorae.hds.utc.fr/api/documents/" + App.logIn + "-PrescriptionData.json");
            //    newListPrescriptions = JsonConvert.DeserializeObject<List<Prescription>>(strJsonPresc);
            //    await FileIO.WriteTextAsync(sampleFilePrescription, JsonConvert.SerializeObject(newListPrescriptions));
            //}
            //else
            //{
            //    sampleFilePrescription = await App.storageFolder.GetFileAsync("PrescriptionData.json");
            //    strJsonPresc = await FileIO.ReadTextAsync(sampleFilePrescription);
            //    newListPrescriptions = JsonConvert.DeserializeObject<List<Prescription>>(strJsonPresc);
            //}

            var storageDirectory = System.IO.Path.Combine(pathFolder, "Consignela");
            Directory.CreateDirectory(storageDirectory);
            sampleFilePrescription = System.IO.Path.Combine(storageDirectory, "PrescriptionData.json");

            if (File.Exists(sampleFilePrescription))
            {
                strJsonPresc = File.ReadAllText(sampleFilePrescription);
                newListPrescriptions = JsonConvert.DeserializeObject<List<Prescription>>(strJsonPresc);
            }

                gridViewPresc.ItemsSource = null;
            gridViewPresc.ItemsSource = newListPrescriptions;
            //newListPrescriptions.Reverse();                
        }

    }
}
