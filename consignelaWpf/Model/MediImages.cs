﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace consignelaWpf.Model
{
    public class MediImages
    {
        public String name { get; set; }

        public String imageUri { get; set; }

        public String enabled { get; set; }

        public float opacity { get; set; }
    }
}
