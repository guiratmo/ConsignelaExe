﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace consignelaWpf
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        public event EventHandler CanExecuteChanged;

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            MessageBox.Show("Selection Binding");
        }

        private void experienceItem_Selected(object sender, RoutedEventArgs e)
        {
            framePages.NavigationService.Navigate(new Uri("pages/PrepareExperience.xaml", UriKind.Relative));
            //framePages.Navigate("Experience.xaml");
        }

        private void parametresItem_Selected(object sender, RoutedEventArgs e)
        {
            framePages.NavigationService.Navigate(new Uri("pages/ConfigPage.xaml", UriKind.Relative));

        }

        private void quit_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Application.Current.Shutdown();
        }

        private void BackButton_Click(object sender, RoutedEventArgs e)
        {
            if (framePages.CanGoBack){framePages.GoBack();}
        }

        private void analyseItem_Selected(object sender, RoutedEventArgs e)
        {
            framePages.NavigationService.Navigate(new Uri("pages/DataPage.xaml", UriKind.Relative));
        }

        private void main_Loaded(object sender, RoutedEventArgs e)
        {
            framePages.NavigationService.Navigate(new Uri("pages/MainPage.xaml", UriKind.Relative));
        }
    }
}
